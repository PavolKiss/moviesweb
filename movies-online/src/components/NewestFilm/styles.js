import Styled from "styled-components";

export const Box = Styled.div`
  display: flex;
  align-items: baseline;
  justify-content: center;
  flex-wrap: wrap;
`;
