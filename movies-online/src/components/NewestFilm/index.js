import React from "react";
import { Box } from "./styles";
import axios from "axios";
import FilmCategory from "../FilmCategory";

class NewestFilms extends React.Component {
  state = {
    data: [],
    loading: true,
    id: ""
  };

  componentWillMount() {
    this.getDataFromNewestFilms();
  }

  getDataFromNewestFilms = async () => {
    try {
      const responce = await axios({
        method: "get",
        url: "/movie/new"
      });

      this.setState({
        data: responce.data,
        loading: false
      });
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const { data } = this.state;
    console.log(data);
    return (
      <Box>
        {this.state.data.map(item => (
          <FilmCategory
            src={item.picture}
            id={item.id}
            categoryName={item.title}
            key={item.id}
          />
        ))}
      </Box>
    );
  }
}

export default NewestFilms;
