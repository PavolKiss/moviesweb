import React from "react";
import { Genre, StyledLink, StyledWrapper } from "./styles";
import axios from "axios";
import { withRouter } from "react-router-dom";

class MoviesContainer extends React.Component {
  state = {
    data: [],
    loading: true,
    id: ""
  };

  componentWillMount() {
    this.geAllGenre();
  }

  geAllGenre = async () => {
    try {
      const response = await axios({
        method: "get",
        url: "/category"
      });
      this.setState(
        {
          data: response.data
        },
        () => {
          this.setState({ loading: false });
          console.log(response.data);
        }
      );
      console.log(response.data);
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const { loading, data } = this.state;
    const { onClick, click, id } = this.props;
    if (loading) {
      return <div>loading</div>;
    }

    return (
      <StyledWrapper>
        <StyledLink to={`/dashboard/allmovies/`}>
          <Genre onClick={click} id="">
            All
          </Genre>
        </StyledLink>
        {data.map(item => (
          <StyledLink key={item.id} to={`/dashboard/allmovies/${id}`}>
            <Genre onClick={onClick} id={item.id} key={item.id}>
              {item.categoryName}
            </Genre>
          </StyledLink>
        ))}
      </StyledWrapper>
    );
  }
}

export default withRouter(MoviesContainer);
