import Styled from "styled-components";
import { NavLink } from "react-router-dom";

export const Genre = Styled.div`
margin-left: 8.89em;
background-color: #393939;
box-shadow: 0 0 9px 2px rgb(0,0,0);
width: 100%;
line-height: 50px;
padding-left: 20px;
color: #f2b134;
display: inline-block;
font-family: 'Noto Sans TC', sans-serif;
transition: all .15s ease-in-out;
&:hover {
    background-color: #000000;
    color: #transition: all .15s ease-in-out;
        &:hover {
        background-color: #f2b134;
        color: #000000;
        };
}
`;

export const StyledWrapper = Styled.div`
width: 11%;
`;

export const StyledLink = Styled(NavLink)``;
