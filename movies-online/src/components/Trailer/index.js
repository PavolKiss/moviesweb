import React from 'react';
import YouTube from 'react-youtube';
import {Wrapper, StyledDiv} from '../Trailer/styles'

class Trailer extends React.Component {
    render() {
    const opts = {
    height: '500',
    width: '850'
    };
    const {ytbUrl} = this.props;
    return (
        <Wrapper>
            <StyledDiv>
                <YouTube
                    videoId={ytbUrl}
                    opts={opts}
                    onReady={this._onReady}
                />
            </StyledDiv>
    </Wrapper>
    );
}

_onReady(event) {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
    }
}

export default Trailer;