import styled from 'styled-components';

export const Wrapper = styled.div`
    width: 100%;
    display: flex;
    align-content: center;
    padding-top: 20px;
    padding-bottom: 30px;
`;

export const StyledDiv = styled.div`
    margin: auto;
`;