import Styled from "styled-components";

export const StyledWrapper = Styled.div`
width:100%;
margin: 1.5em auto;
display: flex;
justify-content: center;
`;
