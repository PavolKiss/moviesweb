import React from "react";
import { StyledWrapper } from "./styles";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import "./style.css";
const style = {
  color: "#ffffff",
  backgroundColor: "#1D1D1D",
  border: "none",
  borderBottom: " 2px solid #f2b134",
  width: "20%",
  padding: "12px 10px 12px 27px",
  outline: "none"
};

const styleicon = {
  color: "#f2b134",
  padding: "12px 10px 12px 27px"
};

const SearchFilter = ({ onChange }) => (
  <StyledWrapper>
    <FontAwesomeIcon icon={faSearch} size="2x" style={styleicon} />
    <input
      className="inputColor"
      style={style}
      onChange={onChange}
      placeholder="Search..."
    />
  </StyledWrapper>
);
export default SearchFilter;
