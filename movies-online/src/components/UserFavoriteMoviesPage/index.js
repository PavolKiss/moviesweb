import React from "react";
import base64 from "base-64";
import Axios from "axios";
import FavoriteMovie from "../FavoriteMovie";

class UserFavoriteMoviesPage extends React.Component {
  state = {
    userID: null,
    data: []
  };

  componentWillMount() {
    const token = JSON.parse(window.sessionStorage.getItem("token")) || null;
    if (token === null) {
      this.history.push("/login");
    } else {
      const parsedToken = token.data.split(".");
      const userId = JSON.parse(base64.decode(parsedToken[1]));
      this.setState({ userID: userId.user_id });
    }
  }

  componentDidMount() {
    this.favoriteMovies();
  }

  favoriteMovies = async () => {
    const userID = this.state.userID;

    try {
      // eslint-disable-next-line
      const response = await Axios({
        method: "get",
        url: `/favorite/${userID}`,
        config: { headers: { "Content-Type": "application/json" } }
      });
      this.setState({
        data: response.data
      });
    } catch (error) {
      console.log(error);
      alert("Something went wrong!");
    }
  };

  render() {
    return (
      <div>
        {this.state.data.map(item => (
          <FavoriteMovie
            src={item.movie.picture}
            id={item.movie.id}
            key={item.movie.id}
          />
        ))}
      </div>
    );
  }
}

export default UserFavoriteMoviesPage;
