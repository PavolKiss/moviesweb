import styled from "styled-components";

export const Label = styled.label`
  display: flex;
  justify-content: center;
  padding-top: 23px;
  font-weight: bold;
  padding-bottom: 5px;
  font-size: 20px;
`;
export const TextArea = styled.textarea`
  font-size: 15px;
  text-indent: 7px;
  box-sizing: border-box;
  border: 3px solid #ccc;
  -webkit-transition: width 0.4s ease-in-out;
  transition: width 0.4s ease-in-out;
  -webkit-transition: 0.5s;
  transition: 0.5s;
  outline: none;

  &:focus {
    width: 40%;
    border: 3px solid #555;
    text-align: center;
  }
`;

export const TextAreaWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

export const Button = styled.button`
  background-color: whitesmoke;
  border: none;
  color: black;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  font-size: 15px;
  font-weight: bold;
  margin: 4px 2px;
  border-radius: 10px;
  border: 2px solid black;
  cursor: pointer;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  outline: none;
  &:hover {
    background: #e6e6e6;
  }
`;
