import React from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { Wrapper } from "../DeleteCategory/styles";
import Axios from "axios";
import updateButton from "../../assets/update.png";
import Modal from "react-responsive-modal";
import UpdateMovieInput from "../UpdateMovieInput";
import { ButtonWrapper } from "../AddMovie/style";
import { Label, TextArea, TextAreaWrapper, Button } from "./styles";

class UpdateMovie extends React.Component {
  state = {
    allMovies: [],
    updateData: {},
    open: false,
    closeOnOverlayClick: true,
    id: null,
    author: "",
    actors: "",
    picture: "",
    title: "",
    url: "",
    categoryName: "",
    categoryID: null,
    year: null,
    ytbUrl: "",
    description: "",
    imdbRating: ""
  };

  componentWillMount() {
    this.fillStateAllMovies();
  }

  fillStateAllMovies = async () => {
    try {
      const response = await Axios({
        method: "get",
        url: `/movie`,
        config: { headers: { "Content-Type": "application/json" } }
      });
      this.setState({ allMovies: response.data });
    } catch (err) {
      console.log(err);
    }
  };

  updateMovie = async e => {
    e.preventDefault();
    const id = this.state.id;
    const form = {
      id: this.state.id,
      author: this.state.author,
      actors: this.state.actors,
      category: {
        categoryName: this.state.categoryName,
        id: this.state.categoryID
      },
      title: this.state.title,
      imdbRating: this.state.imdbRating,
      picture: this.state.picture,
      url: this.state.url,
      year: this.state.year,
      ytbUrl: this.state.ytbUrl,
      description: this.state.description
    };
    try {
      // eslint-disable-next-line
      const response = await Axios({
        method: "put",
        url: `/movie/${id}`,
        data: form,
        config: { headers: { "Content-Type": "application/json" } }
      });
      alert("Movie has been sucessfully updated!");
      window.location.reload();
    } catch (err) {
      console.log(err);
      alert("Error");
      window.location.reload();
    }
  };

  onOpenModal = () => {
    this.setState({ open: true });
    this.setState({ closeOnOverlayClick: false });
    this.setState({ closeOnEsc: false });
  };

  onCloseModal = () => {
    this.setState({ open: false });
  };

  handleValueAuthor = e => {
    this.setState({ author: e.target.value });
  };

  handleValuePicture = e => {
    this.setState({ picture: e.target.value });
  };

  handleValueActors = e => {
    this.setState({ actors: e.target.value });
  };

  handleValueTitle = e => {
    this.setState({ title: e.target.value });
  };

  handleValueCategoryName = e => {
    this.setState({ categoryName: e.target.value });
  };

  handleValueURL = e => {
    this.setState({ url: e.target.value });
  };

  handleValueYear = e => {
    this.setState({ year: e.target.value });
  };

  handleValueYtbURL = e => {
    this.setState({ ytbUrl: e.target.value });
  };

  handleValueDescription = e => {
    this.setState({ description: e.target.value });
  };

  handleValueImdbRating = e => {
    this.setState({ imdbRating: e.target.value });
  };

  updateCell = (cell, row) => {
    return (
      <div
        onClick={() => {
          this.setState({
            id: row.id,
            author: row.author,
            actors: row.actors,
            title: row.title,
            categoryName: row.categoryName,
            categoryID: row.categoryID,
            picture: row.picture,
            url: row.url,
            imdbRating: row.imdbRating,
            year: row.year,
            ytbUrl: row.ytbUrl,
            description: row.description
          });
        }}
      >
        <img
          onClick={this.onOpenModal}
          style={{ width: 25, cursor: "pointer" }}
          src={updateButton}
          alt="update?"
        />
      </div>
    );
  };

  render() {
    const { open, closeOnOverlayClick } = this.state;

    const tableData = this.state.allMovies.map(item => ({
      ...item,
      categoryID: item.category.id,
      categoryName: item.category.categoryName
    }));

    return (
      <Wrapper>
        <BootstrapTable
          ref="table"
          data={tableData}
          dataAlign="center"
          pagination={true}
          hover={true}
        >
          <TableHeaderColumn
            isKey={true}
            dataField="id"
            dataSort={true}
            dataAlign="center"
            filter={{ type: "TextFilter", delay: 1000 }}
          >
            ID
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="author"
            dataSort={true}
            dataAlign="center"
            filter={{ type: "TextFilter", delay: 1000 }}
          >
            Author
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="actors"
            dataSort={true}
            dataAlign="center"
            filter={{ type: "TextFilter", delay: 1000 }}
          >
            Actors
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="title"
            dataSort={true}
            dataAlign="center"
            filter={{ type: "TextFilter", delay: 1000 }}
          >
            Movie Name
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="categoryName"
            dataSort={true}
            dataAlign="center"
            filter={{ type: "TextFilter", delay: 1000 }}
          >
            Category
          </TableHeaderColumn>

          <TableHeaderColumn dataField="categoryID" hidden>
            Category ID
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="url"
            dataSort={true}
            dataAlign="center"
            filter={{ type: "TextFilter", delay: 1000 }}
          >
            Movie URL
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="year"
            dataSort={true}
            dataAlign="center"
            filter={{ type: "TextFilter", delay: 1000 }}
          >
            Year
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="picture"
            dataSort={true}
            dataAlign="center"
            filter={{ type: "TextFilter", delay: 1000 }}
          >
            Picture URL
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="ytbUrl"
            dataSort={true}
            dataAlign="center"
            filter={{ type: "TextFilter", delay: 1000 }}
          >
            Youtube URL
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="description"
            dataSort={true}
            dataAlign="center"
            filter={{ type: "TextFilter", delay: 1000 }}
          >
            Description
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="imdbRating"
            dataSort={true}
            dataAlign="center"
            filter={{ type: "TextFilter", delay: 1000 }}
          >
            Imdb Rating
          </TableHeaderColumn>

          <TableHeaderColumn
            dataField="update"
            dataFormat={this.updateCell}
            dataAlign="center"
          >
            Update
          </TableHeaderColumn>
        </BootstrapTable>

        <div>
          <Modal
            open={open}
            closeOnOverlayClick={closeOnOverlayClick}
            onClose={this.onCloseModal}
            center
          >
            <h2 style={{ width: "800px", textAlign: "center" }}>
              Update Movie
            </h2>
            <Label>Movie Name</Label>
            <form onSubmit={this.updateMovie}>
              <UpdateMovieInput
                type="text"
                value={this.state.title}
                minLength={2}
                onChange={this.handleValueTitle}
              />

              <Label>Director</Label>

              <UpdateMovieInput
                type="text"
                value={this.state.author}
                minLength={2}
                onChange={this.handleValueAuthor}
              />

              <Label>Actors</Label>

              <UpdateMovieInput
                type="text"
                value={this.state.actors}
                minLength={2}
                onChange={this.handleValueActors}
              />

              <Label>Picture URL</Label>
              <UpdateMovieInput
                type="text"
                value={this.state.picture}
                minLength={5}
                onChange={this.handleValuePicture}
              />

              <Label>URL</Label>
              <UpdateMovieInput
                type="text"
                value={this.state.url}
                minLength={2}
                onChange={this.handleValueURL}
              />

              <Label>Year</Label>
              <UpdateMovieInput
                type="number"
                value={this.state.year}
                onChange={this.handleValueYear}
              />

              <Label>Youtube URL</Label>
              <UpdateMovieInput
                type="text"
                value={this.state.ytbUrl}
                minLength={10}
                maxLength={11}
                onChange={this.handleValueYtbURL}
              />

              <Label>Description</Label>
              <TextAreaWrapper>
                <TextArea
                  type="text"
                  value={this.state.description}
                  minLength={20}
                  rows="5"
                  onChange={this.handleValueDescription}
                />
              </TextAreaWrapper>

              <ButtonWrapper>
                <Button>Submit</Button>
              </ButtonWrapper>
            </form>
          </Modal>
        </div>
      </Wrapper>
    );
  }
}

export default UpdateMovie;
