import React from "react";
import { Wrapper, ImgCategory, Title, TitleB } from "./styles";

const FilmCategory = ({ src, categoryName, id }) => (
  <Wrapper to={`/dashboard/moviedetails/${id}`}>
    <ImgCategory src={src} />
    <Title>
      <TitleB>{categoryName}</TitleB>
    </Title>
  </Wrapper>
);

export default FilmCategory;
