import styled from "styled-components";
import { Link } from "react-router-dom";

export const Wrapper = styled(Link)`
  width: calc(20.55%);
  opacity: 0.7;
  text-align: center;
  padding: 1em;
  font-size: 25px;
  text-decoration: none;
  position: relative;
  display: inline-block;
  transition: all 0.2s ease-in-out;
  &:hover {
    opacity: 1;
  }
`;

export const ImgCategory = styled.img`
  width: 100%;
  height: 240px;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
`;
export const Title = styled.div`
  color: white;
  text-align: center;
  width: 100%;
  background: #1e1e1e;
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
  padding-top: 3px;
  padding-right: 0px;
  padding-bottom: 5px;
  padding-left: 0px;
`;

export const TitleB = styled.div`
  width: 100%;
  display: flex;
  align-items: baseline;
  justify-content: center;
  flex-wrap: wrap;
`;
