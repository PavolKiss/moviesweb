import React from "react";
import {
  StyledWrapper,
  Text,
  Border,
  ButtonWrapper,
  StyledButton,
  TextArea
} from "./styles";

const Comment = ({ onChange, onClick }) => (
  <div>
    <Text>LEAVE YOUR COMMENT</Text>
    <Border />
    <StyledWrapper>
      <TextArea onChange={onChange} />
      <ButtonWrapper>
        <StyledButton onClick={onClick}>SEND</StyledButton>
      </ButtonWrapper>
    </StyledWrapper>
  </div>
);

export default Comment;
