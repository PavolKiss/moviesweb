import Styled from "styled-components";

export const StyledWrapper = Styled.div`
width:53%;
margin: 2em auto;
`;

export const Text = Styled.div`
font-family:Noto Sans TC;
font-weight: 200px;
margin-top: 3.5em;
text-align: center;
line-height: 1.1;
color: #EFEFE5;
text-transform: uppercase;
letter-spacing: .05em;
font-size: 20px;
`;

export const Border = Styled.div`
width: 140px;
border-bottom: 3px solid #EFEFE5;
height: 2px;
clear: both;
display: block;
margin: 14px auto;
`;

export const ButtonWrapper = Styled.div`
  padding-top: 10px;
`;

export const StyledButton = Styled.button`
background: #1e1e1e;
border-radius: 5px;
border: 2px solid #f2b134;
color: #D5D6D1;
font-weight: bold;
font-size: 16px;
margin: 3px;
cursor: pointer;
padding: 14px 35px;
display: block;
outline: none;
-webkit-transition: 0.4s;
&:hover {
background-color: #000000;
    color: #transition: all .15s ease-in-out;
         &:hover {
         background-color: #f2b134;
         color: #000000;
        };
 }`;

export const TextArea = Styled.textarea`
  width: 100%;
  border-radius: 16px;
  height: 150px;
  font-family:Noto Sans TC;
  resize: none;
  padding: 0.5rem;
  font-size: 18px;
  color:#EFEFE5;
  background-color: #999999;
  box-shadow: 0 0 3px 2px rgb(250,250,250);
  outline: none;
 `;
