import React from "react";
import { StyledWrapper } from "./styles";
import axios from "axios";
import CommentBox from "../../components/CommentBox";
import Box from "../../components/AllComments/";
import base64 from "base-64";

class PlayerVideo extends React.Component {
  state = {
    id: "",
    data: [],
    dataComments: [],
    loading: true,
    role: "",
    token: "",
    url: "",
    movie: "",
    user: "",
    textArea: ""
  };

  componentWillMount() {
    this.getDataForMovie();
    const id = this.props.match.params.id;
    const token = JSON.parse(window.sessionStorage.getItem("token"));
    const parsedToken = token.data.split(".");
    const data = JSON.parse(base64.decode(parsedToken[1]));
    this.setState({
      movie: id,
      user: data.user_id
    });
    this.getDataFromAllComments(id);
  }

  getDataForMovie = async () => {
    const id = this.props.match.params.id;
    this.setState({ id });

    try {
      const response = await axios({
        method: "get",
        url: `/movie/${id}`
      });
      this.setState({
        data: response.data,
        url: response.data.url
      });
    } catch (e) {
      console.log(e);
    }
  };

  SendMessage = async e => {
    e.preventDefault();
    const data = {
      comment: this.state.textArea,
      movie: this.state.movie,
      user: this.state.user
    };
    try {
      // eslint-disable-next-line
      const response = await axios({
        data: data,
        method: "post",
        url: "/comment/comment"
      });
      alert("Your comment has been added");
      window.location.reload();
    } catch (e) {
      console.log(e);
    }
  };

  getDataFromTextArea = e => {
    this.setState({
      textArea: e.target.value
    });
  };

  getDataFromAllComments = async id => {
    try {
      const response = await axios({
        method: "get",
        url: `/comment/${id}`
      });

      this.setState(
        {
          dataComments: response.data
        },
        () => {
          this.setState({ loading: false });
        }
      );
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const { dataComments } = this.state;
    return (
      <StyledWrapper>
        <iframe
          title="movie"
          src={
            `https://videospider.in/getvideo?key=07NF5sUbfuwiN3mF&video_id=` +
            this.state.url
          }
          width="100%"
          height="500"
          frameBorder="0"
          allowFullScreen={true}
          scrolling="no"
        />
        <div>
          <CommentBox
            onChange={e => {
              this.getDataFromTextArea(e);
            }}
            onClick={this.SendMessage}
          />
          <Box data={dataComments} />
        </div>
      </StyledWrapper>
    );
  }
}

export default PlayerVideo;
