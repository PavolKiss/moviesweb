import React from "react";
import { Wrapper, ImgMovie } from "./styles";

const FavoriteMovie = ({ src, id }) => (
  <Wrapper to={`/dashboard/moviedetails/${id}`}>
    <ImgMovie src={src} />
  </Wrapper>
);

export default FavoriteMovie;
