import styled from "styled-components";
import { Link } from "react-router-dom";

export const Wrapper = styled(Link)`
  width: calc(27.77%);
  text-align: center;
  padding: 1em;
  margin-left: 2px;
  font-size: 25px;
  text-decoration: none;
  position: relative;
  display: inline-block;
  opacity: 0.7;
  color: black;
  transition: 0.5s;

  &:hover {
    opacity: 1;
  }
`;

export const ImgMovie = styled.img`
  width: 90%;
`;
