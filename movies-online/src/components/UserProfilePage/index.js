import React from "react";
import AdminInput from "../../components/AdminInput";
import { StyledLabel } from "../../components/AddMovie/style";
import base64 from "base-64";
import Axios from "axios";
import { Button, ButtonWrapper } from "./styles";

class UserProfilePage extends React.Component {
  state = {
    userID: null,
    username: "",
    email: ""
  };

  componentWillMount() {
    const token = JSON.parse(window.sessionStorage.getItem("token")) || null;
    if (token === null) {
      this.history.push("/login");
    } else {
      const parsedToken = token.data.split(".");
      const userData = JSON.parse(base64.decode(parsedToken[1]));
      this.setState({
        userID: userData.user_id,
        username: userData.user_name,
        email: userData.user_mail
      });
    }
  }

  handleValueUserName = e => {
    this.setState({ username: e.target.value });
  };

  handleValueUserEmail = e => {
    this.setState({ email: e.target.value });
  };

  logOut = () => {
    window.sessionStorage.removeItem("token");
    this.props.history.push("/");
  };

  onSubmit = async e => {
    e.preventDefault();
    const id = this.state.userID;
    const form = {
      username: this.state.username,
      email: this.state.email
    };
    console.log("form", form);

    // eslint-disable-next-line
    var myJSON = JSON.stringify(form);

    try {
      // eslint-disable-next-line
      const response = await Axios({
        method: "put",
        url: `/user/${id}`,
        data: form,
        config: { headers: { "Content-Type": "application/json" } }
      });
      alert("Your profile has been updated! Now you will be logout.");
      this.logOut();
    } catch (error) {
      console.log(error);
      alert("Update failed");
    }
  };

  render() {
    const { username, email } = this.state;
    return (
      <form onSubmit={this.onSubmit}>
        <StyledLabel>User Name</StyledLabel>
        <AdminInput
          type="text"
          value={username}
          minLength={3}
          changeState={this.handleValueUserName}
        />

        <StyledLabel>E-mail</StyledLabel>
        <AdminInput
          type="text"
          value={email}
          minLength={3}
          changeState={this.handleValueUserEmail}
        />

        <ButtonWrapper>
          <Button>Submit</Button>
        </ButtonWrapper>
      </form>
    );
  }
}

export default UserProfilePage;
