import styled from "styled-components";
import { Link } from "react-router-dom";

export const StyledLink = styled(Link)`
  margin-top: 50px;
  margin-left: 570px;
  text-transform: uppercase;
  text-decoration: none;
  display: flex;
  height: 60px;
  line-height: 60px;
  color: white;
  font-size: 1.05em;
  justify-content: center;
  align-items: center;
  transition: all 0.2s linear;
  &:hover {
    text-decoration: none;
    color: #f2b134;
  }
  &:focus {
    text-decoration: none;
  }
`;
export const WelcomeWrapper = styled.div`
  font-family: "Dosis", sans-serif;
  margin-top: 2em;
  text-align: center;
  font-size: 20px;
  color: #808080;
`;

export const StyledWrapper = styled.div`
  background: #333333;
  margin: 3.5em auto;
  padding: 1em;
  width: 50%;
  border-radius: 15px;
  box-shadow: 0px 2px 7px 2px black;
  margin-bottom: 16em;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 15px;
`;

export const Button = styled.button`
  background: #333333;
  border-radius: 5px;
  border: 2px solid #f2b134;
  color: #f2b134;
  font-size: 15px;
  padding: 10px;
  height: 45px;
  display: block;
  outline: none;
  -webkit-transition: 0.4s;
  transition: 0.4s;

  &:hover {
    background: #262626;
  }
`;
