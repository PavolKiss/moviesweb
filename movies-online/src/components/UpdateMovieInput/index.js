import React from 'react';
import {StyledInput, Wrapper} from './styles';

const UpdateMovieInput = ({type, minLength, maxLength, onChange, defaultValue, value}) => (
    <Wrapper>
    <StyledInput 
    type={type}
    onChange={onChange}
    value={value}
    defaultValue={defaultValue}

    required
    minLength={minLength}
    maxLength={maxLength}
    />
    </Wrapper>
);

export default UpdateMovieInput;