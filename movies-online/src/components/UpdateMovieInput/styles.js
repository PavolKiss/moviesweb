import styled from 'styled-components';

export const StyledInput = styled.input`
    height: 30px;
    display:inline-block;
    font-size: 15px;
    text-indent: 7px;
    box-sizing: border-box;
    border: 3px solid #ccc;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
    -webkit-transition: 0.5s;
    transition: 0.5s;
    outline: none;

    &:focus{
        width: 40%;
        height: 40px;
        border: 3px solid #555;
        text-align: center;
    }
`;
export const Wrapper = styled.div`
    display: flex;
    justify-content: center;
`;