import React from "react";
import {
  Wrapper,
  StyledWrapper,
  TextArea,
  WrapperTextArea,
  StyledLabel,
  Button,
  ButtonWrapper,
  YoutubeLinkWrapper,
  Select,
  SelectWrapper
} from "./style";
import AdminInput from "../AdminInput";
import Axios from "axios";

class AddMovie extends React.Component {
  state = {
    actors: "",
    author: "",
    title: "",
    picture: "",
    year: "",
    url: "",
    ytbUrl: "",
    description: "",
    categoryData: [],
    id: null,
    movieTitle: "",
    movieTrailer: "",
    imdbRating: "",
    genre: ""
  };

  componentWillMount() {
    this.fillStateData();
  }

  fillStateMovieInfo = async () => {
    try {
      const response = await Axios({
        method: "get",
        url:
          `http://www.omdbapi.com/?t=` +
          this.state.movieTitle +
          "&apikey=9024e995",
        config: { headers: { "Content-Type": "application/json" } }
      });
      this.setState({
        actors: response.data.Actors,
        author: response.data.Director,
        title: response.data.Title,
        picture: response.data.Poster,
        year: response.data.Year,
        description: response.data.Plot,
        imdbRating: response.data.imdbRating,
        url: response.data.imdbID,
        genre: response.data.Genre
      });
    } catch (error) {
      console.log(error);
    }
  };

  fillYoutubeLink = async () => {
    try {
      const response = await Axios({
        method: "get",
        url:
          `https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&q=` +
          this.state.movieTrailer +
          `+++++++&type=video+++++++++&videoDefinition=high&key=AIzaSyCwJ9ii7wNBNwbsCsaTKsm4L-HFfbYnXFo`,
        config: { headers: { "Content-Type": "application/json" } }
      });
      this.setState({ ytbUrl: response.data.items[0].id.videoId });
    } catch (error) {
      console.log(error);
    }
  };

  fillStateData = async () => {
    try {
      const response = await Axios.get("/category");
      this.setState({
        categoryData: response.data
      });
    } catch (err) {
      console.log(err);
    }
  };

  handleAuthor = e => {
    this.setState({ author: e.target.value });
  };

  handleTitle = e => {
    this.setState({ title: e.target.value });
  };

  handlePicture = e => {
    this.setState({ picture: e.target.value });
  };

  handleYear = e => {
    this.setState({ year: e.target.value });
  };

  handleURL = e => {
    this.setState({ url: e.target.value });
  };

  handleYtbUrl = e => {
    this.setState({ ytbUrl: e.target.value });
  };

  handleDescription = e => {
    this.setState({ description: e.target.value });
  };

  handleOptionValue = e => {
    const index = e.nativeEvent.target.selectedIndex;
    this.setState({
      id: e.nativeEvent.target[index].id
    });
  };

  handleSearch = e => {
    this.setState({ movieTitle: e.target.value });
  };

  handleSearchMovieTrailer = e => {
    this.setState({ movieTrailer: e.target.value });
  };

  handleActors = e => {
    this.setState({ actors: e.target.value });
  };

  handleImdbRating = e => {
    this.setState({ imdbRating: e.target.value });
  };

  onSubmit = async event => {
    event.preventDefault();
    const category = {
      id: this.state.id
    };
    const form = {
      author: this.state.author,
      actors: this.state.actors,
      title: this.state.title,
      category: category,
      year: this.state.year,
      picture: this.state.picture,
      url: this.state.url,
      ytbUrl: this.state.ytbUrl,
      imdbRating: this.state.imdbRating,
      description: this.state.description
    };

    try {
      // eslint-disable-next-line
      const response = await Axios({
        method: "post",
        url: "/movie/add",
        data: form,
        config: { headers: { "Content-Type": "application/json" } }
      });
      alert("Movie has been sucesffully added!");
      window.location.reload();
    } catch (error) {
      console.log(error);
      alert("Something went wrong, please try again.");
      window.location.reload();
    }
  };

  render() {
    const {
      author,
      title,
      year,
      url,
      ytbUrl,
      description,
      loading,
      movieTitle,
      movieTrailer,
      actors,
      picture,
      imdbRating,
      genre
    } = this.state;

    if (loading) {
      return (
        <Wrapper>
          <form onSubmit={this.onSubmit}>
            <StyledWrapper>
              <StyledLabel>Director</StyledLabel>
              <AdminInput
                type="text"
                value={author}
                minLength={3}
                maxLength={25}
                changeState={this.handleAuthor}
              />

              <StyledLabel>Actors</StyledLabel>
              <AdminInput
                type="text"
                value={actors}
                minLength={3}
                maxLength={25}
                changeState={this.handleAuthor}
              />

              <StyledLabel>Movie Name</StyledLabel>
              <AdminInput
                type="text"
                value={title}
                minLength={2}
                changeState={this.handleTitle}
              />

              <StyledLabel>Poster</StyledLabel>
              <AdminInput
                type="text"
                value={picture}
                minLength={5}
                changeState={this.handlePicture}
              />

              <StyledLabel>Category</StyledLabel>
              <SelectWrapper>
                <Select defaultValue="" onChange={this.handleOptionValue}>
                  <option value="" key="default" disabled>
                    Select one of the following category...
                  </option>
                  {this.state.categoryData.map(item => (
                    <option id={item.id} key={item.id}>
                      {item.categoryName}
                    </option>
                  ))}
                </Select>
              </SelectWrapper>
              <div
                style={{ textAlign: "center", color: "#b3b3b3", fontSize: 15 }}
              >
                ({genre})
              </div>

              <StyledLabel>Year</StyledLabel>
              <AdminInput
                type="number"
                value={year}
                changeState={this.handleYear}
              />

              <StyledLabel>Movie URL</StyledLabel>
              <AdminInput
                type="text"
                value={url}
                placeholder="https://www.movie-url.com"
                changeState={this.handleURL}
              />

              <StyledLabel>Youtube Video ID</StyledLabel>
              <AdminInput
                type="text"
                value={ytbUrl}
                placeholder="watch?v=VIDEO_ID"
                maxLength={11}
                changeState={this.handleYtbUrl}
              />
              <YoutubeLinkWrapper
                style={{ color: "white", opacity: 0.9, fontSize: 15 }}
              />

              <StyledLabel>Imdb Rating</StyledLabel>
              <AdminInput
                type="text"
                value={imdbRating}
                maxLength={3}
                changeState={this.handleImdbRating}
              />

              <StyledLabel>Description</StyledLabel>
              <WrapperTextArea>
                <TextArea
                  value={description}
                  placeholder="Enter description here..."
                  onChange={this.handleDescription}
                  cols="40"
                  rows="6"
                  required
                  minLength={20}
                />
              </WrapperTextArea>
            </StyledWrapper>

            <ButtonWrapper>
              <Button>Submit</Button>
            </ButtonWrapper>
          </form>
        </Wrapper>
      );
    }

    const enabled = movieTitle.length > 0 && movieTrailer.length > 5;

    return (
      <Wrapper>
        <StyledWrapper>
          <StyledLabel>Search for Movie</StyledLabel>
          <AdminInput
            type="text"
            value={movieTitle}
            changeState={this.handleSearch}
            placeholder="Please enter movie name for more details..."
          />

          <StyledLabel>Search for Trailer</StyledLabel>
          <AdminInput
            type="text"
            value={movieTrailer}
            changeState={this.handleSearchMovieTrailer}
            placeholder="Please enter movie name for search video ID..."
          />
        </StyledWrapper>

        <ButtonWrapper style={{ paddingBottom: 354 }}>
          <Button
            onClick={() => {
              this.setState({ loading: true });
              this.fillStateMovieInfo();
              this.fillYoutubeLink();
            }}
            disabled={!enabled}
          >
            Search
          </Button>
        </ButtonWrapper>
      </Wrapper>
    );
  }
}

export default AddMovie;
