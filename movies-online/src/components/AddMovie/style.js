import styled from "styled-components";

export const Wrapper = styled.div`
  padding: 5px 450px 25px 450px;
`;

export const StyledWrapper = styled.div`
  background: #333333;
  margin: auto;
  padding: 1em;
  padding-top: 25px;
  padding-bottom: 35px;
  width: 90%;
  border-radius: 15px;
  box-shadow: 0px 2px 7px 2px black;
`;

export const TextArea = styled.textarea`
  font-size: 15px;
  outline: none;
  border: none;
  background: #333333;
  border-bottom: 2px solid #f2b134;
  opacity: 0.4;
  -webkit-transition: 0.5s;
  transition: 0.5s;
  color: #ffffff;
  font-size: 17px;

  &::placeholder {
    color: #b3b3b3;
    opacity: 1;
    outline: none;
  }

  &:focus {
    opacity: 1;
  }
`;

export const WrapperTextArea = styled.div`
  display: flex;
  justify-content: center;
  padding-top: 15px;
  width: 100%;
`;

export const StyledLabel = styled.label`
  position: absolute;
  margin-top: 20px;
  margin-left: 100px;
  font-size: 18px;
  display: flex;
  justify-content: center;
  color: #666666;
  font-weight: bold;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 25px 0px 15px 0px;
`;

export const Button = styled.button`
  background-color: #1e1e1e;
  border: none;
  color: #f2b134;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  font-size: 15px;
  font-weight: bold;
  margin: 4px 2px;
  border-radius: 10px;
  border: 2px solid #f2b134;
  cursor: pointer;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  outline: none;
  &:hover {
    background: #262626;
  }
`;

export const TextWrapper = styled.div`
  text-align: center;
  font-size: 17px;
  margin-top: 10px;
  margin-bottom: 5px;
`;
export const YoutubeLinkWrapper = styled.div`
  font-size: 17px;
  text-align: center;
`;

export const Select = styled.select`
  width: 40%;
  height: 30px;
  display: inline-block;
  font-size: 15px;
  text-indent: 7px;
  border: none;
  background: #333333;
  border-bottom: 2px solid #f2b134;
  outline: none;
  opacity: 0.4;
  color: #ffffff;
  font-size: 17px;

  &:focus {
    opacity: 1;
  }
`;
export const SelectWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding-top: 15px;
`;
