import styled from 'styled-components';
import { NavLink } from "react-router-dom";

const StyledLinkComponent = styled(NavLink)`
    margin-top: 3px;
    text-transform: uppercase;
    text-decoration: none;
    display: flex;
    height: 60px;
    line-height: 60px;
    width: 100%;
    color: white;
    background: #606060;
    font-size: 1.05em;
    justify-content: center;
    align-items: center;
    transition: all 0.2s linear;
    border-right: 1px solid #454545;
    &:last-child{
        border-right: 0;
    }
    &:hover {
        text-decoration: none;
        color: #f2b134;
    }
    &:focus {
        text-decoration: none;
    }
`;
export default StyledLinkComponent;