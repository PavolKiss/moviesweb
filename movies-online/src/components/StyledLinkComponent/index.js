import React from 'react';
import StyledLinkComponent from './styles';

const StyledLink = ({activeClass, href, children}) => (
    <StyledLinkComponent 
        activeStyle={activeClass} to = {href}>
        {children}
    </StyledLinkComponent>
)

export default StyledLink;