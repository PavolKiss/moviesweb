import styled from "styled-components";

const StyledWrapperSubHeader = styled.div`
  z-index: 10000;
  margin: auto;
  display: flex;
  flex-direction: row;
`;

export default StyledWrapperSubHeader;
