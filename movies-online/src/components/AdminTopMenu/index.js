import React from "react";
import StyledWrapperSubHeader from "./styles";
import StyledLinkComponent from "../../components/StyledLinkComponent";

const activeClass = { background: "#454545", color: "#f2b134" };
class SubHeader extends React.Component {
  state = {
    link: [
      { id: 0, href: "dashboard/adminpage/addmovie", name: "Add Movie" },
      { id: 1, href: "dashboard/adminpage/deletemovie", name: "Delete Movie" },
      { id: 2, href: "dashboard/adminpage/updatemovie", name: "Update Movie" },
      { id: 3, href: "dashboard/adminpage/addcategory", name: "Add Category" },
      {
        id: 4,
        href: "dashboard/adminpage/deletecategory",
        name: "Delete Category"
      }
    ]
  };
  render() {
    const { link } = this.state;
    return (
      <StyledWrapperSubHeader>
        {link.map(item => (
          <StyledLinkComponent
            activeClass={activeClass}
            href={`/${item.href}`}
            key={item.id}
          >
            {item.name}
          </StyledLinkComponent>
        ))}
      </StyledWrapperSubHeader>
    );
  }
}
export default SubHeader;
