import React from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import '../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import 'bootstrap/dist/css/bootstrap.css';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'
import deleteButton from '../../assets/delete.png';
import axios from 'axios';
import { Wrapper } from './styles';

class DeleteCategory extends React.Component{

    state = {
        allCategories: []
    }

    componentWillMount(){
        this.fillStateAllCategories();
    }

    fillStateAllCategories = async () => {
        try {
            const response = await axios({
                method: "get",
                url: `/category`,
                config: { headers: { "Content-Type": "application/json" } }
            });
            this.setState({ allCategories: response.data});
        } catch (err) {
            console.log(err);
        }
    };

    deleteCategory = async (id) => {
		try {
            // eslint-disable-next-line
			const response = await axios({
				method: 'delete',
				url: `/category/${id}`,
				config: { headers: { 'Content-Type': 'application/json' } }
			});
            window.location.reload();
		} catch (err) {
            console.log(err);
            alert('Error');
		}
    }

    deleteCell = (cell, row) => {
        return(
            <div
                onClick={()=>{
                    confirmAlert({
                        title: 'Confirm to delete',
                        message: 'Are you sure delete this movie?',
                        buttons: [
                    {
                        label: 'Yes',
                        onClick: () => this.deleteCategory(row.id)
                    },
                    {
                        label: 'No',
                        onClick: () => window.location.reload()

                    }
                    ]
                    })
                }}
                >
                <img style={{width: 25, cursor:'pointer'}} src={deleteButton} alt="" />
                </div>
        );
    }

    render(){
        return(
            <Wrapper>
                <BootstrapTable ref='table' data={this.state.allCategories} dataAlign="center" pagination={ true } hover={true}>
                    <TableHeaderColumn isKey={true} dataField='id' dataSort={ true } dataAlign="center" filter={ { type: 'TextFilter', delay: 1000 } }>ID</TableHeaderColumn>
                    <TableHeaderColumn dataField='categoryName' dataSort={ true } dataAlign="center" filter={ { type: 'TextFilter', delay: 1000 } }>Category Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='id' dataFormat={this.deleteCell} dataAlign="center">Delete</TableHeaderColumn>
                </BootstrapTable>
            </Wrapper>
        );
    }
}

export default DeleteCategory;