import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  height: 70vh;
  display: flex;
  background: url(${props => props.bg});
  background-size: cover;
`;
