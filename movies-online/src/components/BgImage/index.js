import React from "react";
import bgImg from "../../assets/bgImg.jpg";
import { Wrapper } from "./styles";

const BgImage = () => <Wrapper bg={bgImg} />;

export default BgImage;
