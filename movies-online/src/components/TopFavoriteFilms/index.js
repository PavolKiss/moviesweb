import React from "react";
import axios from "axios";
import { Box } from "../NewestFilm/styles";
import FilmCategory from "../FilmCategory";

class TopFavoriteFilms extends React.Component {
  state = {
    data: [],
    loading: false,
    id: "",
    categoryName: ""
  };

  componentWillMount() {
    this.getDataFromFavoriteFilm();
  }

  getDataFromFavoriteFilm = async () => {
    try {
      const responce = await axios({
        method: "get",
        url: "/movie/fav"
      });

      this.setState({
        data: responce.data,
        loading: false
      });
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const { data } = this.state;
    console.log(data);
    return (
      <Box>
        {this.state.data.map(item => (
          <FilmCategory
            src={item.picture}
            id={item.id}
            categoryName={item.title}
            key={item.id}
          />
        ))}
      </Box>
    );
  }
}
export default TopFavoriteFilms;
