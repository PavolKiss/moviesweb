import styled from "styled-components";
import { Link } from "react-router-dom";

export const HeaderWrapper = styled.div`
  height: 100px;
  width 100%;
  background: #454545;
  box-shadow: 0 -2px 7px 5px black;
  &:hover {
    color: #f04933;
  }
`;
export const LogoHeader = styled.img`
  height: 70px;
  float: left;
  padding-left: 60px;
  padding-top: 10px;
`;
export const StyledLink = styled(Link)`
  color: white;
  text-decoration: none;
  float: left;
  padding-left: 75px;
  padding-top: 32px;
  font-size: 25px;
  transition: all 0.2s ease-in-out;
  &:hover {
    color: #f2b134;
  }
`;
export const StyledLogLink = styled(Link)`
  padding-right: 100px;
  padding-top: 32px;
  font-size: 25px;
  color: white;
  float: right;
  transition: all 0.2s ease-in-out;
  text-decoration: none;
  font-size: 25px;
  &:hover {
    color: #f2b134;
  }
`;

export const StyledDiv = styled.div`
  color: white;
  float: right;
  line-height: 100px;
  padding: 0 50px;
  text-decoration: none;
  font-size: 25px;
  cursor: pointer;
  &:hover {
    color: #f2b134;
  }
`;
