import React from "react";
import logo from "../../assets/logo.png";
import {
  HeaderWrapper,
  LogoHeader,
  StyledLink,
  StyledLogLink,
  StyledDiv
} from "./styles";
import { withRouter } from "react-router-dom";
import base64 from "base-64";

class Header extends React.Component {
  state = {
    isLoged: false,
    role: "",
    token: ""
  };

  componentWillMount() {
    const token = JSON.parse(window.sessionStorage.getItem("token")) || null;
    if (token) {
      const parsedToken = token.data.split(".");
      // eslint-disable-next-line
      const role = JSON.parse(base64.decode(parsedToken[1]));
      this.setState({ role: JSON.parse(base64.decode(parsedToken[1])) });
      this.setState({
        token: JSON.parse(window.sessionStorage.getItem("token"))
      });
    }

    if (token !== null) {
      this.setState({ isLoged: true });
    }
  }

  logOut = () => {
    this.setState({ isLoged: false });
    window.sessionStorage.removeItem("token");
    this.props.history.push("/");
  };

  render() {
    if (this.state.isLoged) {
      if (
        this.state.role.auth[0].authority === "ROLE_USER" &&
        this.state.token !== null
      ) {
        return (
          <HeaderWrapper>
            <LogoHeader src={logo} />
            <StyledLink to="/">Home</StyledLink>
            <StyledLink to="/dashboard/allmovies">All Movies</StyledLink>
            <StyledDiv onClick={this.logOut}>Log Out</StyledDiv>
            <StyledLogLink to="/dashboard/userprofile/basicinfo">
              Profile
            </StyledLogLink>
          </HeaderWrapper>
        );
      } else if (
        this.state.role.auth[0].authority === "ROLE_ADMIN" &&
        this.state.token !== null
      ) {
        return (
          <HeaderWrapper>
            <LogoHeader src={logo} />
            <StyledLink to="/">Home</StyledLink>
            <StyledLink to="/dashboard/allmovies">All Movies</StyledLink>
            <StyledDiv onClick={this.logOut}>Log Out</StyledDiv>
            <StyledLogLink to="/dashboard/adminpage">Admin Page</StyledLogLink>
          </HeaderWrapper>
        );
      }
    }

    return (
      <HeaderWrapper>
        <LogoHeader src={logo} />
        <StyledLink to="/">Home</StyledLink>
        <StyledLink to="/dashboard/allmovies">All Movies</StyledLink>
        <StyledLogLink to="/registration">Sign Up</StyledLogLink>
        <StyledLogLink to="/login">Login</StyledLogLink>
      </HeaderWrapper>
    );
  }
}

export default withRouter(Header);
