import styled from "styled-components";
import { NavLink } from "react-router-dom";

export const Wrapper = styled.div`
  float: left;
  padding: 65px 25px 50px 50px;
`;
export const ImgMovie = styled.img``;

export const ButtonPlay = styled.button`
  font-family: "Dosis", sans-serif;
  background: whitesmoke;
  outline: none;
  letter-spacing: 2px;
  width: 240px;
  padding: 12px 55px;
  color: #000;
  border: 2px solid #ffffff;
  float: left;
  border-radius: 35px;
  margin-top: 10px;
  margin-left: 10px;
  margin-bottom: 10px;
  text-align: center;
  text-decoration: none;
  font-weight: 800;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  cursor: pointer;

  &:hover {
    background: #d9d9d9;
    border: 2px solid #d9d9d9;
  }

  &:disabled {
    background: #292929;
    color: #9b9b9b;
    border: 2px solid #292929;
    cursor: not-allowed;

    &:hover {
      background: #404040;
      border: 2px solid #404040;
    }
  }
`;

export const StyledButton = styled(NavLink)``;
