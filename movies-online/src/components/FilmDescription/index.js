import React from "react";
import {
  StyledWrapper,
  BorderWrapper,
  AboutFilm,
  Title,
  Picture,
  Year,
  ContainerWrapper,
  Author,
  Description,
  StyledLink
} from "./styles";

const styles = {
  color: "#f5f5f5",
  fontFamily: "Noto Sans TC"
};

const styleT = {
  color: "#f2b134",
  fontFamily: "Noto Sans TC"
};

const FilmDescription = ({ item, src }) => (
  <StyledLink to={`/dashboard/moviedetails/${item.id}`}>
    <StyledWrapper>
      <BorderWrapper>
        <AboutFilm>
          <Picture src={src} />
          <ContainerWrapper>
            <Title style={styleT}>{item.title}</Title>
            <Year style={styles}>{item.year}</Year>
            <Author style={styles}>{item.author}</Author>
            <Description style={styles}>{item.description}</Description>
          </ContainerWrapper>
        </AboutFilm>
      </BorderWrapper>
    </StyledWrapper>
  </StyledLink>
);

export default FilmDescription;
