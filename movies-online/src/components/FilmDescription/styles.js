import Styled from "styled-components";
import { NavLink } from "react-router-dom";
import styled from "styled-components";

export const StyledWrapper = Styled.div`
width: 80%;
margin: 2em auto;
margin-left: 7em;
`;

export const BorderWrapper = Styled.div`
width: 81.4%;
float: right;
box-shadow: 0 0 9px 2px rgb(0,0,0);
display: inline-block;
`;

export const AboutFilm = Styled.div`
height: 15.625em;
background-color: #333333;
padding-bottom: 1.250em;
padding-top: 1.250em;
padding-left: 0.500em;
font-weight: 300;
font-size: 16px;
display: flex;
`;

export const ContainerWrapper = Styled.div`
width: 70%;`;

export const Picture = Styled.img`
width:25%;
height: 250px;
margin-left: 15px;
margin-bottom: 10px;
transition: all .2s linear;
border-radius: 10px;
display: inline-block;
transition: all 0.2s ease-in-out;
&:hover{
    transform: scale(1.08);
    }
`;

export const Title = Styled.div`
width: 100%;
font-size: 1.5em;
height: 35px;
padding-left: 17px;
display:block;
font-weight: bold;
`;

export const Year = Styled.div`
 width: 100%;
 font-size: 1em;
 height: 25px;
 padding-left: 17px;
 display:block;
`;
export const Author = Styled.div`
 width: 100%;
 font-size: 1em;
 height: 25px;
 padding-left:17px;
 display:block;`;

export const Description = Styled.div`
 width: 95%;
 font size: 1em;
 height: 25px;
 padding-left: 17px;
 display:inline-block;
 `;

export const StyledLink = styled(NavLink)``;
