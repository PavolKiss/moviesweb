import styled from "styled-components";

export const StyledFooter = styled.div`
  display: flex;
  justify-content: center;
  position: relative;
  right: 0;
  bottom: 0;
  left: 0;
  padding: 1rem;
  background-color: #f2b134;
  text-align: center;
  box-shadow: 0 -2px 7px 0px black;
  clear: bottom;
  max-width: 100%;
`;

export const StyledText = styled.div`
  width: 100%;
  color: #454545;
  font-size: 11px;
  text-align: center;
  letter-spacing: 1px;
`;
