import styled from "styled-components";

export const StyledLabel = styled.div`
  position: absolute;
  margin-top: 20px;
  margin-left: 120px;
  font-size: 18px;
  display: flex;
  justify-content: center;
  color: #808080;
  font-weight: bold;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 25px 0px 312px 0px;
`;
export const Button = styled.button`
  background-color: #1e1e1e;
  border: none;
  color: #f2b134;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  font-size: 15px;
  font-weight: bold;
  margin: 50px 2px;
  border-radius: 10px;
  border: 2px solid #f2b134;
  cursor: pointer;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  outline: none;
  &:hover {
    background: #262626;
  }
`;
