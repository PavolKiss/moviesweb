import React from 'react';
import AdminInput from '../AdminInput';
import { Wrapper, StyledWrapper} from '../AddMovie/style';
import {StyledLabel, Button, ButtonWrapper } from './styles';
import Axios from 'axios';

class AddCategory extends React.Component{
    state = {
        categoryName: ""
    };

    categoryName = e => {
        this.setState({categoryName: e.target.value})
    };

    onSubmit = async e => {
        e.preventDefault();

        const form = {
            categoryName: this.state.categoryName
        };

        try {
            // eslint-disable-next-line
            const response = await Axios({
                method: "post",
                url: "/category",
                data: form,
                config: {headers: {"Content-Type": "application/json"}}
            });
            alert("Category has been sucesffully added!");
            window.location.reload();
        } catch (error) {
            console.log(error);
            alert("Something went wrong, please try again.");
            window.location.reload();
        }
    };

    render() {
        const {categoryName} = this.state;
        return(
        <Wrapper>
                <form onSubmit = {this.onSubmit}>
                <StyledWrapper>

                <StyledLabel>
                    Category Name 
                </StyledLabel>    
                <AdminInput 
                type="text"
                minLength={3}
                value={categoryName} 
                changeState={this.categoryName}
                />
                </StyledWrapper>

            <ButtonWrapper>
            <Button>Submit</Button>
            </ButtonWrapper>
            </form>
            </Wrapper>
        )
    }
}

export default AddCategory;