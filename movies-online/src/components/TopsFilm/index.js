import React from "react";
import axios from "axios";
import { Box } from "../NewestFilm/styles";
import FilmCategory from "../FilmCategory";

class TopsFilms extends React.Component {
  state = {
    data: [],
    loading: false,
    id: ""
  };

  componentWillMount() {
    this.getDataFromTopsFilm();
  }

  getDataFromTopsFilm = async () => {
    try {
      const responce = await axios({
        method: "get",
        url: "/movie/top"
      });

      this.setState({
        data: responce.data,
        loading: false
      });
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const { data } = this.state;
    console.log(data);
    return (
      <Box>
        {this.state.data.map(item => (
          <FilmCategory
            src={item.movie.picture}
            id={item.movie.id}
            categoryName={item.movie.title}
            key={item.movie.id}
          />
        ))}
      </Box>
    );
  }
}

export default TopsFilms;
