import React from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import deleteButton from '../../assets/delete.png';
import axios from 'axios';
import { Wrapper } from '../DeleteCategory/styles';

class DeleteCategory extends React.Component{

    state = {
        allMovies: []
    }

    componentWillMount(){
        this.fillStateAllMovies();
    }

    fillStateAllMovies = async () => {
        try {
            const response = await axios({
                method: "get",
                url: `/movie`,
                config: { headers: { "Content-Type": "application/json" } }
            });
            this.setState({allMovies: response.data});
        } catch (err) {
            console.log(err);
        }
    };

    deleteMovie = async (id) => {
		try {
            // eslint-disable-next-line
			const response = await axios({
				method: 'delete',
				url: `/movie/${id}`,
				config: { headers: { 'Content-Type': 'application/json' } }
			});
            window.location.reload();
		} catch (err) {
            console.log(err);
            alert('Error');
		}
    }

    deleteCell = (cell, row) => {
        return(
            <div
                onClick={()=>{
                    confirmAlert({
                        title: 'Confirm to delete',
                        message: 'Are you sure delete this movie?',
                        buttons: [
                    {
                        label: 'Yes',
                        onClick: () => this.deleteMovie(row.id)
                    },
                    {
                        label: 'No',
                        onClick: () => window.location.reload()

                    }
                    ]
                    })
                }}
                >
                <img style={{width: 25, cursor:'pointer'}} src={deleteButton} alt="delete?" />
                </div>
        );
    }

    render(){
        const tableData = this.state.allMovies.map(item => ({
            ...item,
            categoryName: item.category.categoryName
        }))
        return(
                <Wrapper>
                <BootstrapTable id='table' ref='table' data={tableData} pagination={true} hover={true}>
                    <TableHeaderColumn isKey={true} dataField='id' dataAlign="center" dataSort={ true } filter={ { type: 'TextFilter', delay: 1000 } }>ID</TableHeaderColumn>
                    <TableHeaderColumn dataField='title' dataAlign="center" dataSort={ true } filter={ { type: 'TextFilter', delay: 1000 } }>Title</TableHeaderColumn>
                    <TableHeaderColumn dataField='categoryName' dataAlign="center" dataSort={ true } filter={ { type: 'TextFilter', delay: 1000 } }>Category</TableHeaderColumn>
                    <TableHeaderColumn dataField='year' dataAlign="center" dataSort={ true } filter={ { type: 'TextFilter', delay: 1000 } }>Year</TableHeaderColumn>
                    <TableHeaderColumn dataField='delete' dataAlign="center" dataFormat={this.deleteCell}>Delete</TableHeaderColumn>
                </BootstrapTable>
                </Wrapper>
        );
    }
}

export default DeleteCategory;