import React from "react";
import {
  StyledWrapper,
  Content,
  MovieTitle,
  Year,
  Author,
  Wrapper,
  ContentWrapper,
  Category,
  Actors,
  Rating
} from "./styles";

const ContentFilm = ({
  content,
  movieTitle,
  year,
  author,
  category,
  actors,
  imdbRating
}) => (
  <div>
    <StyledWrapper>
      <MovieTitle>{movieTitle}</MovieTitle>

      <Wrapper>
        <Year>{year}</Year>
        <Category>{category}</Category>
        <Author> Director: {author}</Author>
        <Actors>Actors: {actors}</Actors>
      </Wrapper>

      <ContentWrapper>
        <Content>{content}</Content>
        <Rating>
          Imdb:{" "}
          <span style={{ color: "#f2b134", fontWeight: "bold" }}>
            {imdbRating}
          </span>
        </Rating>
      </ContentWrapper>
    </StyledWrapper>
  </div>
);

export default ContentFilm;
