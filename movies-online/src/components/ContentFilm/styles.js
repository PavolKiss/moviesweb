import styled from "styled-components";
import { NavLink } from "react-router-dom";

export const StyledWrapper = styled.div`
  padding: 55px 50px 45px;
`;
export const Content = styled.div`
  font-size: 17px;
  color: #9f9f9f;
  letter-spacing: 1px;
  line-height: 1.5;
  padding-top: 30px;
  font-family: "Dosis", sans-serif;
`;

export const MovieTitle = styled.div`
  font-size: 25px;
  font-weight: bold;
  color: #f2b134;
  font-family: "Dosis", sans-serif;
`;

export const StyledLink = styled(NavLink)``;

export const Year = styled.div`
  font-size: 17px;
  color: whitesmoke;
  font-family: "Dosis", sans-serif;
`;

export const Author = styled.div`
  font-size: 17px;
  color: whitesmoke;
  font-family: "Dosis", sans-serif;
`;
export const Wrapper = styled.div`
  padding-top: 10px;
`;
export const ContentWrapper = styled.div`
  margin-top: 20px;
`;
export const Category = styled.div`
  font-size: 17px;
  color: whitesmoke;
  font-family: "Dosis", sans-serif;
`;

export const Actors = styled.div`
  font-size: 17px;
  color: whitesmoke;
  font-family: "Dosis", sans-serif;
`;

export const Rating = styled.div`
  font-size: 20px;
  color: whitesmoke;
  margin-top: 20px;
  text-transform: uppercase;
  text-align: right;
  font-family: "Dosis", sans-serif;
  text-decoration: bold;
`;
