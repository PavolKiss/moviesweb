import React from "react";
import {
  Box,
  Text,
  Border,
  StyledWrapper,
  UserName,
  Date,
  Comment
} from "./styles";
import Moment from "react-moment";

class AllComments extends React.Component {
  state = {
    data: [],
    loading: true
  };

  render() {
    const { data } = this.props;
    return (
      <div>
        <Text>COMMENTS</Text>
        <Border />
        <Box>
          {data.map(item => (
            <StyledWrapper key={item.id}>
              <UserName>{item.user.username}</UserName>
              <Date>
                <Moment format="LLL">{item.date}</Moment>
              </Date>
              <Comment>{item.comment}</Comment>
            </StyledWrapper>
          ))}
        </Box>
      </div>
    );
  }
}
export default AllComments;
