import Styled from "styled-components";

export const Box = Styled.div`
width:60%;
position: relative;
margin: 3.5em auto
background: #999999;
-webkit-box-shadow: 0 4px 80px #07d7b80;
box-shadow: 0 4px 80px #7d7b80;
padding: 2rem;
border-radius: 16px;
`;

export const Text = Styled.div`
font-family:Noto Sans TC;
font-weight: 200px;
text-align: center;
line-height: 1.1;
color: #EFEFE5;
text-transform: uppercase;
letter-spacing: .05em;
font-size: 20px;
`;

export const Border = Styled.div`
width: 170px;
border-bottom: 3px solid #EFEFE5;
height: 2px;
clear: both;
display: block;
margin: 14px auto;
`;

export const StyledWrapper = Styled.div`
display: flex;
flex-direction:column;
justify-content: center;
align-items: baseline;
flex-wrap: wrap;
`;

export const UserName = Styled.div`
width: 100%;
font-family: Lato,Helvetica Neue,Helvetica,Arial,sans-serif;
font-size: 2em;
height: 35px;
padding-left: 11px;
display:block;
font-weight: bold;
`;

export const Date = Styled.div`
width: 100%;
font-size: 0.8em;
height: 30px;
padding-left: 12px;
display:block;
`;

export const Comment = Styled.div`
 width: 100%;
 font-family:Noto Sans TC;
 font-size: 1.5em;
 height: 25px;
 padding-left: 15px;
 display:block;
 color: #EFEFE5
 margin-bottom: 6%;
 `;
