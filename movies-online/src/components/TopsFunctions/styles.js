import styled from "styled-components";
import { NavLink } from "react-router-dom";

export const StyledWrapper = styled.div`
  width: 100%;
  display: inline-block;
  transition: all 0.2s ease-in-out;
`;

export const StyledLink = styled(NavLink)`
  text-decoration: none;
  display: inline-block;
  text-align: center;
  color: rgb(0, 0, 0);
  width: 33.33%;
  border: 0;
  height: 65px;
  line-height: 70px;
  text-transform: uppercase;
  font-size: 19px;
  letter-spacing: 1px;
  border-top-right-radius: 20px;
  border-top-left-radius: 20px;
  cursor: pointer;
  position: relative;
  background-color: #f2b134;
  color: #454545;
  transition: all 0.2s ease-in-out;
  box-shadow: 0 0 9px 2px rgb(0, 0, 0);
  &:hover {
    text-decoration: none;
    color: #f5f5f5;
  }
  &:focus {
    text-decoration: none;
  }
`;
