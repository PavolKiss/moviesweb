import React from "react";
import { StyledWrapper, StyledLink } from "./styles";

const TopsFunctions = () => (
  <StyledWrapper>
    <StyledLink
      to="/dashboard/homepage/new"
      activeStyle={{
        background: "#454545",
        color: "#f5f5f5",
        fontSize: 18,
        fontWeight: "bold"
      }}
    >
      NEW MOVIES
    </StyledLink>
    <StyledLink
      to="/dashboard/homepage/tops"
      activeStyle={{
        background: "#454545",
        color: "#f5f5f5",
        fontSize: 18,
        fontWeight: "bold"
      }}
    >
      TODAY'S TOP
    </StyledLink>
    <StyledLink
      to="/dashboard/homepage/favorite"
      activeStyle={{
        background: "#454545",
        color: "#f5f5f5",
        fontSize: 18,
        fontWeight: "bold"
      }}
    >
      FAVORITE TOP
    </StyledLink>
  </StyledWrapper>
);

export default TopsFunctions;
