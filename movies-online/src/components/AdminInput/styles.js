import styled from "styled-components";

export const StyledInput = styled.input`
  width: 40%;
  height: 30px;
  display: inline-block;
  font-size: 15px;
  text-indent: 7px;
  color: #ffffff;
  border: none;
  background: #333333;
  border-bottom: 2px solid #f2b134;
  outline: none;
  -webkit-transition: 0.5s;
  transition: 0.5s;
  opacity: 0.4;
  font-size: 17px;

  &::placeholder {
    color: #b3b3b3;
    opacity: 1;
    outline: none;
    font-size: 16px;
  }

  &:focus {
    opacity: 1;
  }
`;
export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  padding-top: 15px;
`;
