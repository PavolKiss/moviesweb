import React from 'react';
import { StyledInput, Wrapper } from './styles';

const AdminInput = ({type, placeholder, value, changeState, minLength, maxLength}) => (
    <Wrapper>
        <StyledInput 
        type={type}
        placeholder={placeholder} 
        value={value}
        onChange={changeState}

        required
        minLength={minLength}
        maxLength={maxLength}
        />
    </Wrapper>
)

export default AdminInput;