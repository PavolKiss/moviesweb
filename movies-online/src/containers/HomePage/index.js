import React from "react";
import BgImage from "../../components/BgImage";
import TopsFunctions from "../../components/TopsFunctions";
import { Wrapper, Border } from "./styles";
import Axios from "axios";
import { Switch, Route } from "react-router-dom";
import NewestFilms from "../../components/NewestFilm";
import TopsFilms from "../../components/TopsFilm";
import TopFavoriteFilms from "../../components/TopFavoriteFilms";

class HomePage extends React.Component {
  state = {
    categoryData: []
  };

  componentWillMount() {
    this.fillStateData();
  }

  fillStateData = async () => {
    try {
      const response = await Axios.get("/category");
      this.setState({
        categoryData: response.data
      });
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    return (
      <div>
        <BgImage />
        <Border>
          <TopsFunctions />
        </Border>
        <Wrapper>
          <Switch>
            <Route path="/dashboard/homepage/tops" component={TopsFilms} />
            <Route path="/dashboard/homepage/new" component={NewestFilms} />
            <Route
              path="/dashboard/homepage/favorite"
              component={TopFavoriteFilms}
            />
          </Switch>
        </Wrapper>
      </div>
    );
  }
}

export default HomePage;
