import styled from "styled-components";

export const Border = styled.div`
  width: 60%;
  position: relative;
  margin-left: auto;
  margin-right: auto;
  bottom: 35px;
`;

export const Styled = styled.div`
  width: 100%;
  display: inline-block;
  transition: all 0.2s ease-in-out;
`;

export const Wrapper = styled.div`
  background: #454545;
  margin: 0 auto 70px;
  max-width: 60%;
  position: relative;
  bottom: 35px;
  box-shadow: 0 0 9px 2px rgb(0, 0, 0);
`;
