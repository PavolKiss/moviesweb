import React from 'react';
import AdminTopMenu from '../../components/AdminTopMenu';
import {Switch, Route, Redirect} from 'react-router-dom';
import AddMovie from '../../components/AddMovie';
import AddCategory from '../../components/AddCategory';
import UpdateMovie from '../../components/UpdateMovie';
import DeleteMovie from '../../components/DeleteMovie';
import DeleteCategory from '../../components/DeleteCategory';
import Wrapper from './styles';

class AdminPage extends React.Component{

    render(){
        return(
            <div>
                <Wrapper>
                    <AdminTopMenu/>
                </Wrapper>
                    <Switch>
                    <Route
                    exact
                    path="/dashboard/adminpage"
                    render={() => <Redirect to="/dashboard/adminpage/addmovie" />}
                    />

                    <Route path="/dashboard/adminpage/addmovie" component={AddMovie}/>
                    <Route path="/dashboard/adminpage/deletemovie" component={DeleteMovie} />
                    <Route path="/dashboard/adminpage/updatemovie" component={UpdateMovie} />
                    <Route path="/dashboard/adminpage/addcategory" component={AddCategory} />
                    <Route path="/dashboard/adminpage/deletecategory" component={DeleteCategory} />
                    </Switch>
            </div>
        );
    }
}

export default AdminPage;