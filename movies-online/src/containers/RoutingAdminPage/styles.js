import styled from 'styled-components';

const Wrapper = styled.div `
    margin: auto;
    padding: 1em;
    margin-top: 5px;
    width: 50%;
`;

export default Wrapper;