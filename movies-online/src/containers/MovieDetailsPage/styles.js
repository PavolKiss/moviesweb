import styled from "styled-components";
import { Link } from "react-router-dom";

export const StyledWrapper = styled.div`
  background: #333333;
  margin: 5em auto;
  padding: 1em;
  width: 50%;
  border-radius: 15px;
  box-shadow: 0px 2px 7px 2px black;
`;
export const ButtonWrapper = styled.div``;
export const Button = styled.button`
  font-family: "Dosis", sans-serif;
  background: whitesmoke;
  outline: none;
  letter-spacing: 2px;
  width: 240px;
  padding: 12px 55px;
  color: #000;
  border: 2px solid #ffffff;
  float: left;
  border-radius: 35px;
  margin-top: 10px;
  margin-left: 70px;
  margin-bottom: 10px;
  text-align: center;
  text-decoration: none;
  font-weight: 800;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  cursor: pointer;

  &:hover {
    background: #d9d9d9;
    border: 2px solid #d9d9d9;
  }

  &:disabled {
    background: #292929;
    color: #9b9b9b;
    border: 2px solid #292929;
    cursor: not-allowed;

    &:hover {
      background: #404040;
      border: 2px solid #404040;
    }
  }
`;

export const DeleteButton = styled.button`
  font-family: "Dosis", sans-serif;
  background: whitesmoke;
  text-transform: uppercase;
  outline: none;
  letter-spacing: 2px;
  width: 240px;
  padding: 13px 20px;
  color: #000;
  border: 2px solid #ffffff;
  float: left;
  border-radius: 35px;
  margin-top: 10px;
  margin-left: 70px;
  margin-bottom: 10px;
  text-align: center;
  text-decoration: none;
  font-weight: 800;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  cursor: pointer;

  &:hover {
    background: #d9d9d9;
    border: 2px solid #d9d9d9;
  }

  &:disabled {
    background: #292929;
    color: #9b9b9b;
    border: 2px solid #292929;
    cursor: not-allowed;

    &:hover {
      background: #404040;
      border: 2px solid #404040;
    }
  }
`;

export const Wrapper = styled.div`
  text-align: center;
  margin-right: 80px;
  color: #9b9b9b;
  font-size: 15px;
`;
export const StyledLink = styled(Link)`
  text-decoration: none;
  color: #9b9b9b;
  font-weight: bold;

  &:hover {
    color: #404040;
  }
`;
