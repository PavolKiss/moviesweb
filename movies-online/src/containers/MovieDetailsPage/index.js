import React from "react";
import ContentFilm from "../../components/ContentFilm";
import Trailer from "../../components/Trailer";
import ImgMovieDetails from "../../components/ImgMovieDetails";
import {
  StyledWrapper,
  Button,
  DeleteButton,
  ButtonWrapper,
  Wrapper,
  StyledLink
} from "./styles";
import axios from "axios";
import base64 from "base-64";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  StyledButton,
  ButtonPlay
} from "../../components/ImgMovieDetails/styles";

class MovieDetailsPage extends React.Component {
  state = {
    data: {},
    id: "",
    categoryID: null,
    categoryName: "",
    userID: "",
    picture: "",
    enabled: true,
    disabled: false,
    favoriteUserMovies: [],
    bomba: false,
    imdbRating: ""
  };

  componentWillMount() {
    this.getDataForMovie();
    const token = JSON.parse(window.sessionStorage.getItem("token")) || null;
    if (token === null) {
      this.setState({ enabled: false });
    } else {
      const parsedToken = token.data.split(".");
      const userId = JSON.parse(base64.decode(parsedToken[1]));
      this.setState({ userID: userId.user_id });
    }
  }

  componentDidMount() {
    const token = JSON.parse(window.sessionStorage.getItem("token")) || null;
    if (token === null) {
      this.setState({ enabled: false });
    } else {
      this.favoriteMovies();
    }
  }

  forceUpdate() {
    this.addFavouriteMoviesToUser();
  }

  getDataForMovie = async () => {
    const id = this.props.match.params.id;
    this.setState({ id });

    try {
      const response = await axios({
        method: "get",
        url: `/movie/${id}`
      });
      this.setState({
        data: response.data,
        categoryName: response.data.category.categoryName,
        categoryID: response.data.category.id,
        picture: response.data.picture,
        imdbRating: response.data.imdbRating
      });
    } catch (e) {
      console.log(e);
    }
  };

  favoriteMovies = async () => {
    const userID = this.state.userID;

    try {
      // eslint-disable-next-line
      const response = await axios({
        method: "get",
        url: `/favorite/${userID}`,
        config: { headers: { "Content-Type": "application/json" } }
      });
      this.setState({
        favoriteUserMovies: response.data
      });
      this.dataMap();
    } catch (error) {
      console.log(error);
      alert("Something went wrong!");
    }
  };

  toogleMouseEnter = () => {
    const token = JSON.parse(window.sessionStorage.getItem("token")) || null;
    if (token === null) {
      this.setState({ disabled: true });
    }
  };

  addFavouriteMoviesToUser = async () => {
    const id = this.state.id;
    const userID = this.state.userID;

    try {
      // eslint-disable-next-line
      const response = await axios({
        method: "get",
        url: `/favorite/${id}/${userID}`
      });
      alert("Your movie has been added!");
      window.location.reload();
    } catch (error) {
      console.error(error);
      alert("Something went wrong!");
    }
  };

  dataMap = () => {
    const { favoriteUserMovies, data } = this.state;

    const objectData = data.id;
    const mapArray = favoriteUserMovies
      .map(item => item.movie.id)
      .filter(mapArray => parseInt(mapArray) === data.id);

    if (parseInt(mapArray) === objectData) {
      this.setState({ bomba: true });
    } else {
      this.setState({ bomba: false });
    }
  };

  deleteFavoriteMovies = async e => {
    e.preventDefault();
    const userID = this.state.userID;
    const id = this.state.id;

    try {
      // eslint-disable-next-line
      const response = await axios({
        method: "delete",
        url: `/favorite/${id}/${userID}`,
        config: { headers: { "Content-Type": "application/json" } }
      });
      alert("Movie has been deleted from favorites!");
      window.location.reload();
    } catch (error) {
      console.log(error);
      alert("Something went wrong!");
      window.location.reload();
    }
  };

  render() {
    const { data, id, categoryName, picture } = this.state;
    return (
      <div>
        <StyledWrapper>
          <div style={{ width: "100%" }}>
            <ImgMovieDetails src={picture} />

            <ContentFilm
              content={data.description}
              movieTitle={data.title}
              author={data.author}
              actors={data.actors}
              year={data.year}
              category={categoryName}
              imdbRating={data.imdbRating}
              id={id}
            />
          </div>

          <ButtonWrapper onMouseEnter={this.toogleMouseEnter}>
            <StyledButton to={`/dashboard/player/${id}`}>
              <ButtonPlay disabled={!this.state.enabled}>
                <FontAwesomeIcon style={{ marginRight: "5px" }} icon="play" />
                PLAY
              </ButtonPlay>
            </StyledButton>

            {this.state.bomba === true ? (
              <DeleteButton onClick={this.deleteFavoriteMovies}>
                <FontAwesomeIcon
                  style={{ marginRight: "5px" }}
                  icon="trash-alt"
                />
                Remove from favorites
              </DeleteButton>
            ) : (
              <Button
                onClick={() => {
                  this.addFavouriteMoviesToUser();
                }}
                disabled={!this.state.enabled}
              >
                <FontAwesomeIcon style={{ marginRight: "5px" }} icon="heart" />
                FAVORITE
              </Button>
            )}

            <Wrapper hidden={!this.state.disabled}>
              This function is for{" "}
              <StyledLink to="/registration">registered</StyledLink> members
              only!
            </Wrapper>
          </ButtonWrapper>

          <Trailer ytbUrl={data.ytbUrl} />
        </StyledWrapper>
      </div>
    );
  }
}

export default MovieDetailsPage;
