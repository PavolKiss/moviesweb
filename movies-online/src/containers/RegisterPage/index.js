import React from "react";
import FormInput from "../../components/FormInput";
import {
  StyledWrapper,
  InputWrapper,
  IconWrapper,
  LogoWrapper,
  LogoLogin,
  ButtonWrapper,
  StyledButton,
  ButtonHome,
  ButtonLogin,
  Wrapper,
  Error,
  RecaptchaWrapper
} from "../../components/FormInput/styles";
import logoLogin from "../../assets/logoLogin.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Axios from "axios";
import Recaptcha from "react-recaptcha";

function validate(username, email, password, confirmPassword) {
  const errors = [];

  if (username.length < 3) {
    errors.push("Username should be at least 3 characters long");
  }
  if (password.length !== confirmPassword.length) {
    errors.push("Password and confirm password must be the same!");
  }
  if (email.length < 5) {
    errors.push("Email should be at least 5 characters long");
  }
  if (email.split("").filter(x => x === "@").length !== 1) {
    errors.push("Email should contain a @");
  }
  if (email.indexOf(".") === -1) {
    errors.push("Email should contain at least one dot");
  }

  if (password.length < 6) {
    errors.push("Password should be at least 6 characters long");
  }

  return errors;
}

class RegisterPage extends React.Component {
  state = {
    username: "",
    password: "",
    confirmPassword: "",
    email: "",
    errors: [],
    allowSend: false
  };

  handleUsername = e => {
    this.setState({ username: e.target.value });
  };

  handlePassword = e => {
    this.setState({ password: e.target.value });
  };

  handleConfirmPassword = e => {
    this.setState({ confirmPassword: e.target.value });
  };

  handleEmail = e => {
    this.setState({ email: e.target.value });
  };

  verifyCallback = response => {
    if (response.length > 0) {
      this.setState({ allowSend: true });
    }
  };

  onSubmit = async event => {
    event.preventDefault();
    const form = {
      username: this.state.username,
      password: this.state.password,
      email: this.state.email
    };
    const { username, email, password, confirmPassword } = this.state;

    const errors = validate(username, email, password, confirmPassword);
    if (errors.length > 0) {
      this.setState({ errors });
      return;
    }
    // eslint-disable-next-line
    var myJSON = JSON.stringify(form);

    try {
      // eslint-disable-next-line
      const response = await Axios({
        method: "post",
        url: "/user/registration",
        data: form,
        config: { headers: { "Content-Type": "application/json" } }
      });
      alert("You have sucessfully registered.");
      this.props.history.push("/login");
    } catch (error) {
      alert("Something went wrong. Try again.");
      window.location.reload();
    }
  };

  render() {
    const { username, password, confirmPassword, email, errors } = this.state;
    return (
      <StyledWrapper>
        <LogoWrapper>
          <LogoLogin src={logoLogin} />
        </LogoWrapper>
        <form onSubmit={this.onSubmit}>
          {errors.map(error => (
            <Error key={error}>Error: {error}</Error>
          ))}
          <InputWrapper>
            <IconWrapper>
              <FontAwesomeIcon
                style={{ color: "rgb(217, 217, 217)", margin: "5px" }}
                icon="user"
              />
            </IconWrapper>
            <FormInput
              changeState={this.handleUsername}
              type="text"
              placeholder="User name"
              value={username}
            />
          </InputWrapper>

          <InputWrapper>
            <IconWrapper>
              <FontAwesomeIcon
                style={{ color: "rgb(217, 217, 217)", margin: "10px" }}
                icon="lock"
              />
            </IconWrapper>
            <FormInput
              changeState={this.handlePassword}
              type="password"
              placeholder="Password"
              value={password}
              maxLength={50}
            />
          </InputWrapper>

          <InputWrapper>
            <IconWrapper>
              <FontAwesomeIcon
                style={{ color: "rgb(217, 217, 217)", margin: "10px" }}
                icon="lock"
              />
            </IconWrapper>
            <FormInput
              changeState={this.handleConfirmPassword}
              type="password"
              placeholder="Confirm password"
              value={confirmPassword}
              maxLength={50}
            />
          </InputWrapper>

          <InputWrapper>
            <IconWrapper>
              <FontAwesomeIcon
                style={{ color: "rgb(217, 217, 217)", margin: "10px" }}
                icon="envelope"
              />
            </IconWrapper>
            <FormInput
              changeState={this.handleEmail}
              type="email"
              placeholder="E-mail"
              value={email}
              maxLength={50}
            />
          </InputWrapper>
          <RecaptchaWrapper>
            <Recaptcha
              sitekey="6Lf_9lQUAAAAACJrqzvTHkKhkwjvmKEkEZRsZLvV"
              lang="en"
              theme="light"
              type="image"
              verifyCallback={this.verifyCallback}
            />
          </RecaptchaWrapper>
            <ButtonWrapper>
              <StyledButton>Register</StyledButton>
            </ButtonWrapper>
        </form>
        <Wrapper>
          <ButtonHome to="/">
            <FontAwesomeIcon style={{ marginRight: "5px" }} icon="undo" />
            Home
          </ButtonHome>
          <ButtonLogin to="/login">Login</ButtonLogin>
        </Wrapper>
      </StyledWrapper>
    );
  }
}

export default RegisterPage;
