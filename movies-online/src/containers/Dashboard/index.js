import React from "react";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import AllMovies from "../AllMovies";
import IconLinks from "../../components/IconLinks";
import HomePage from "../HomePage";
import MovieDetailsPage from "../../containers/MovieDetailsPage";
import { Switch, Route } from "react-router-dom";
import RoutingAdminPage from "../RoutingAdminPage";
import RoutingUserProfile from "../RoutingUserProfilePage";
import UserFavoriteMoviesPage from "../../components/UserFavoriteMoviesPage";
import PlayerVideo from "../../components/PlayerVideo";

class Dashboard extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route path="/dashboard/homepage" component={HomePage} />
          <Route path="/dashboard/allmovies" component={AllMovies} />
          <Route path="/dashboard/userprofile" component={RoutingUserProfile} />
          <Route
            path="/dashboard/userprofile/favorites"
            component={UserFavoriteMoviesPage}
          />
          <Route
            path="/dashboard/moviedetails/:id"
            component={MovieDetailsPage}
          />
          <Route path="/dashboard/adminpage" component={RoutingAdminPage} />
          <Route path="/dashboard/player/:id" component={PlayerVideo} />
        </Switch>
        <IconLinks />
        <Footer />
      </div>
    );
  }
}

export default Dashboard;
