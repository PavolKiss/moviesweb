import React from "react";
import { Switch, Route } from "react-router-dom";
import UserProfilePage from "../../components/UserProfilePage";
import UserFavoriteMoviesPage from "../../components/UserFavoriteMoviesPage";
import {
  StyledLink,
  WelcomeWrapper
} from "../../components/UserProfilePage/styles";
import StyledWrapperSubHeader from "../../components/AdminTopMenu/styles";
import base64 from "base-64";
import StyledWrapper from "./styles";

class RoutingUserProfile extends React.Component {
  state = {
    userID: null,
    userName: ""
  };

  componentWillMount() {
    const token = JSON.parse(window.sessionStorage.getItem("token")) || null;
    if (token === null) {
      this.history.push("/login");
    } else {
      const parsedToken = token.data.split(".");
      const userData = JSON.parse(base64.decode(parsedToken[1]));
      this.setState({
        userID: userData.user_id,
        userName: userData.user_name
      });
    }
  }

  render() {
    return (
      <div>
        <WelcomeWrapper>
          Welcome and have a nice day <b>{this.state.userName}</b>!
        </WelcomeWrapper>

        <StyledWrapperSubHeader>
          <StyledLink to="/dashboard/userprofile/basicinfo">
            Basic Info
          </StyledLink>

          <StyledLink to="/dashboard/userprofile/favorites">
            Favorites
          </StyledLink>
        </StyledWrapperSubHeader>

        <Switch>
          <StyledWrapper>
            <Route
              path="/dashboard/userprofile/basicinfo"
              component={UserProfilePage}
            />
            <Route
              path="/dashboard/userprofile/favorites"
              component={UserFavoriteMoviesPage}
            />
          </StyledWrapper>
        </Switch>
      </div>
    );
  }
}

export default RoutingUserProfile;
