import styled from "styled-components";

const StyledWrapper = styled.div`
  background: #333333;
  margin: 3.5em auto;
  padding: 1em;
  width: 50%;
  border-radius: 15px;
  box-shadow: 0px 2px 7px 2px black;
  margin-bottom: 18em;
`;

export default StyledWrapper;
