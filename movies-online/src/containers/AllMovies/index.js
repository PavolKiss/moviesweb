import React from "react";
import { StyledWrapper } from "./styles";
import FilmDescription from "../../components/FilmDescription";
import MoviesContainer from "../../components/MoviesContainer";
import axios from "axios";
import SearchFilter from "../../components/SearchFilter";

class AllMovies extends React.Component {
  state = {
    data: [],
    loading: true,
    dataCopy: [],
    id: null
  };

  componentWillMount() {
    this.getAllMovies();
  }

  getAllMovies = async () => {
    try {
      const response = await axios({
        method: "get",
        url: "/movie"
      });
      this.setState(
        {
          data: response.data
        },
        () => {
          this.setState({ loading: false, dataCopy: response.data });
        }
      );
      console.log(response.data);
    } catch (e) {
      console.log(e);
    }
  };

  search = e => {
    let arr = [];
    // eslint-disable-next-line
    this.state.data.find((item, index) => {
      if (
        item.title
          .toLowerCase()
          .trim()
          .includes(e.target.value.toLowerCase().trim())
      ) {
        arr[index] = item;
      }
    });
    this.setState({ dataCopy: arr });
  };

  getCategoryData = async id => {
    console.log(id);
    this.setState({ id });
    try {
      const response = await axios({
        method: "get",
        url: `/movie/category/${id}`
      });
      this.setState({ loading: false, dataCopy: response.data });
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const { loading, dataCopy, id } = this.state;
    if (loading) {
      return <div>loading</div>;
    }

    return (
      <div>
        <SearchFilter onChange={this.search} />

        {dataCopy.map(item => (
          <FilmDescription
            onClick={e => {
              this.getFilmData(e.target.id);
            }}
            item={item}
            key={item.id}
            src={item.picture}
            onChange={this.search}
          />
        ))}
        <StyledWrapper>
          <MoviesContainer
            id={id}
            click={this.getAllMovies}
            onClick={e => {
              this.getCategoryData(e.target.id);
            }}
          />
        </StyledWrapper>
      </div>
    );
  }
}

export default AllMovies;
