import React from "react";
import FormInput from "../../components/FormInput";
import {
  StyledWrapper,
  InputWrapper,
  IconWrapper,
  StyledButton,
  ButtonWrapper,
  LogoLogin,
  LogoWrapper,
  Wrapper,
  ButtonHome,
  ButtonLogin
} from "../../components/FormInput/styles";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import logoLogin from "../../assets/logoLogin.png";
import Axios from "axios";
import base64 from "base-64";

class LoginPage extends React.Component {
  state = {
    username: "",
    password: "",
    daco: ""
  };

  handleUserName = e => {
    this.setState({ username: e.target.value });
  };
  handlePassword = e => {
    this.setState({ password: e.target.value });
  };

  parseTokenAndRedirectUser = () => {
    const token = JSON.parse(window.sessionStorage.getItem("token"));
    const parsedToken = token.data.split(".");
    const role = JSON.parse(base64.decode(parsedToken[1]));
    if (role.auth[0].authority === "ROLE_ADMIN") {
      this.props.history.push("/");
    } else if (role.auth[0].authority === "ROLE_USER") {
      this.props.history.push("/dashboard/userprofile/basicinfo");
    }
  };

  onSubmit = async event => {
    event.preventDefault();

    const form = new FormData();
    form.append("username", `${this.state.username}`);
    form.append("password", `${this.state.password}`);
    try {
      // eslint-disable-next-line
      const response = await Axios({
        method: "post",
        url: "/user/login",
        data: form,
        config: {
          headers: { "Content-Type": "application/json;charset=UTF-8" }
        }
      });
      window.sessionStorage.setItem("token", JSON.stringify(response));
      window.sessionStorage.setItem(
        "password",
        JSON.stringify(this.state.password)
      );
      this.parseTokenAndRedirectUser();
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { username, password } = this.state;

    return (
      <StyledWrapper>
        <LogoWrapper>
          <LogoLogin src={logoLogin} />
        </LogoWrapper>
        <form onSubmit={this.onSubmit}>
          <InputWrapper>
            <IconWrapper>
              <FontAwesomeIcon
                style={{ color: "rgb(217, 217, 217)", margin: "10px" }}
                icon="user"
              />
            </IconWrapper>

            <FormInput
              onSubmit={this.onSubmit}
              changeState={this.handleUserName}
              type="text"
              placeholder="username"
              value={username}
            />
          </InputWrapper>

          <InputWrapper>
            <IconWrapper>
              <FontAwesomeIcon
                style={{ color: "rgb(217, 217, 217)", margin: "10px" }}
                icon="lock"
              />
            </IconWrapper>

            <FormInput
              onSubmit={this.onSubmit}
              changeState={this.handlePassword}
              type="password"
              placeholder="password"
              value={password}
            />
          </InputWrapper>

          <ButtonWrapper>
            <StyledButton type="submit">Login</StyledButton>
          </ButtonWrapper>
        </form>
        <Wrapper>
          <ButtonHome to="/">
            <FontAwesomeIcon style={{ marginRight: "5px" }} icon="undo" />
            Home
          </ButtonHome>
          <ButtonLogin to="/registration">Sign Up</ButtonLogin>
        </Wrapper>
      </StyledWrapper>
    );
  }
}

export default LoginPage;
